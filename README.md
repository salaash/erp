### About the repository ###

Basic laravel app that captures citizens basic infomation,dependents,education infomation and properties owned.

### How to set up? ###

* Git clone the application files
* cd to cloned application folder
* run **cp .env.example .env** command to create .env file
* access .env file and change database configs
* run **php artisan migrate** command to create database tables
* run **composer install** command to install application dependencies
* run **php artisan key:generate** command
* From there, access application in your browser
