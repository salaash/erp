<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\input;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\PropertyType;

class PropertyTypeController extends Controller
{
    public function index()
    {
        $property_types = PropertyType::all();
        return response()->json($property_types);
    }

    public function store(Request $request)
    {

        $uid = time();

        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:property_type'
        ]);
        if ($validator->passes()) {
            $property_type = new PropertyType;
            $property_type->name = Input::get('name');
            $property_type->property_type_id = $uid;
            $property_type->save();
            return response()->json(['success' => 'Property type saved successfully!']);
        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function show($id)
    {
        return response()->json(PropertyType::findOrFail($id));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:property_type,name,'.$id
        ]);
        if ($validator->passes()) {
            PropertyType::findOrFail($id)
            ->update([
                'name'=>$request->name
                ]);
             return response()->json(['success' => 'Property type updated successfully!']);

        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function destroy($id)
    {
        //
    }
}
