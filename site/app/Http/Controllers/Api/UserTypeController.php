<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\input;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\UserType;

class UserTypeController extends Controller
{
    public function index()
    {
        $user_types = UserType::all();
        return response()->json($user_types);
    }

    public function store(Request $request)
    {
        $uid = time();

        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:user_type'
        ]);
        if ($validator->passes()) {
            $user_type = new UserType;
            $user_type->name = Input::get('name');
            $user_type->user_type_id = $uid;
            $user_type->save();
            return response()->json(['success' => 'User type saved successfully!']);
        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function show($id)
    {
        return response()->json(UserType::findOrFail($id));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:user_type,name,'.$id
        ]);
        if ($validator->passes()) {
            UserType::findOrFail($id)
            ->update([
                'name'=>$request->name
                ]);
             return response()->json(['success' => 'User type updated successfully!']);

        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function destroy($id)
    {
        //
    }
}
