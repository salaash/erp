<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Property;
use App\Models\Citizen;
use App\Models\PropertyType;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\input;
use Illuminate\Support\Facades\Validator;
use DB;
use Carbon\Carbon;

class PropertyController extends Controller
{
    public function index()
    {
        $properties = DB::table('property')
            ->leftJoin('property_type', 'property.property_type_id', '=', 'property_type.property_type_id')
            ->leftJoin('user','property.user_id', '=', 'user.user_id')
            ->select('property.id as test','property.property_id','user.first_name','user.second_name','user.last_name','user.id as user_id','property_type.name','property_description')
            ->get();
        return response()->json($properties);
    }

    public function store(Request $request)
    {

        $uid = time();

        $validator = Validator::make($request->all(), [
            'user'=>'required',
            'property_type'=>'required',
            'property_description'=>'required'
        ]);
        if ($validator->passes()) {
            $property = new Property;
            $property->property_id = $uid;
            $property->user_id = input::get('user');
            $property->property_type_id = input::get('property_type');
            $property->property_description = input::get('property_description');
            $property->save();
            return response()->json(['success' => 'Property saved successfully!']);
        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function show($id)
    {

        $property = DB::table('property')
            ->leftJoin('property_type', 'property.property_type_id', '=', 'property_type.property_type_id')
            ->leftJoin('user','property.user_id', '=', 'user.user_id')
            ->select('property.id','property.property_id','user.first_name','user.second_name','user.last_name','user.user_id','property_type.name','property.property_type_id','property_description')
            ->where('property.id',$id)
            ->get();
        return response()->json($property);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'user'=>'required',
            'property_type'=>'required',
            'property_description'=>'required'
        ]);
        if ($validator->passes()) {
            Property::findOrFail($id)
            ->update([
                'user_id'=>$request->user,
                'property_type_id'=>$request->property_type,
                'property_description'=>$request->property_description
                ]);
             return response()->json(['success' => 'Property updated successfully!']);

        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function destroy($id)
    {
        //
    }
    public function count()
    {
        return response(Property::count());
    }
}
