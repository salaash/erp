<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Education;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\input;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use DB;

class EducationController extends Controller
{
    public function index()
    {
        $properties = DB::table('education')
            ->leftJoin('user', 'education.user_id', '=', 'user.user_id')
            ->leftJoin('school','education.school_id', '=', 'school.school_id')
            ->leftJoin('school_type','school.school_type_id', '=','school_type.school_type_id')
            ->select('education_id','user.first_name','user.second_name','user.last_name','school.name','education.id','school_type.name as school_type_name')
            ->get();
        return response()->json($properties);
    }

    public function store(Request $request)
    {
        $uid = time();

        $validator = Validator::make($request->all(), [
            'school'=>'required',
            'citizen'=>'required'
        ]);
        if ($validator->passes()) {
            $education = new Education;
            $education->education_id = $uid;
            $education->user_id = input::get('citizen');
            $education->school_id = input::get('school');
            $education->save();
            return response()->json(['success' => 'Education saved successfully!']);
        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function show($id)
    {

        // $property = DB::table('property')
        //     ->leftJoin('property_type', 'property.property_type_id', '=', 'property_type.property_type_id')
        //     ->leftJoin('user','property.user_id', '=', 'user.user_id')
        //     ->select('property.id','property.property_id','user.first_name','user.second_name','user.last_name','user.user_id','property_type.name','property.property_type_id','property_description')
        //     ->where('property.id',$id)
        //     ->get();
        // return response()->json($property);
    }

    public function update(Request $request, $id)
    {
        // $validator = Validator::make($request->all(), [
        //     'user'=>'required',
        //     'property_type'=>'required',
        //     'property_description'=>'required'
        // ]);
        // if ($validator->passes()) {
        //     Property::findOrFail($id)
        //     ->update([
        //         'user_id'=>$request->user,
        //         'property_type_id'=>$request->property_type,
        //         'property_description'=>$request->property_description
        //         ]);
        //      return response()->json(['success' => 'Property updated successfully!']);

        // }
        // return response()->json(['errors' => $validator->errors()]);
    }

    public function destroy($id)
    {
        //
    }
}
