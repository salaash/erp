<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SchoolType;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\input;
use Illuminate\Support\Facades\Validator;
use DB;
use Carbon\Carbon;

class SchoolTypeController extends Controller
{
    public function index()
    {
        $school_types = DB::table('school_type')
            ->select('id','name','school_type_id')
            ->get();
        return response()->json($school_types);
    }

    public function store(Request $request)
    {

        $uid = time();

        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:school_type'
        ]);
        if ($validator->passes()) {
            $school_type = new SchoolType;
            $school_type->name = Input::get('name');
            $school_type->school_type_id = $uid;
            $school_type->save();
            return response()->json(['success' => 'School type saved successfully!']);
        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function show($id)
    {
        return response()->json(SchoolType::findOrFail($id));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:school_type,name,'.$id
        ]);
        if ($validator->passes()) {
            SchoolType::findOrFail($id)
            ->update([
                'name'=>$request->name
                ]);
             return response()->json(['success' => 'School type updated successfully!']);

        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function destroy($id)
    {
        //
    }
}
