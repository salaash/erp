<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DependentType;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\input;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class DependentTypeController extends Controller
{

    public function index()
    {
        $dependent_types = DependentType::all();
        return response()->json($dependent_types);
    }

    public function store(Request $request)
    {

        $uid = time();

        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:dependent_type'
        ]);
        if ($validator->passes()) {
            $dependent_type = new DependentType;
            $dependent_type->name = Input::get('name');
            $dependent_type->dependent_Type_id = $uid;
            $dependent_type->save();
            return response()->json(['success' => 'Dependent type saved successfully!']);
        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function show($id)
    {
        return response()->json(DependentType::findOrFail($id));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:dependent_Type,name,'.$id
        ]);
        if ($validator->passes()) {
            DependentType::findOrFail($id)
            ->update([
                'name'=>$request->name
                ]);
             return response()->json(['success' => 'Dependent type updated successfully!']);

        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function destroy($id)
    {
        //
    }
}
