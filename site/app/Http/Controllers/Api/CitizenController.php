<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Citizen;
use App\Models\Property;
use App\Models\PhoneNumber;
use App\Models\Dependent;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\input;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use DB;

class CitizenController extends Controller
{
    public function index()
    {
        $citizens = DB::table('user')
            ->leftJoin('phone_number', 'user.user_id', '=','phone_number.user_id')
            ->select('user.user_id','first_name','second_name','last_name','identity_no','phone_number.phone_no','county_id','occupation','birth')
            ->get();
        return response()->json($citizens);
    }

    public function store(Request $request)
    {

        $uid = time();

        $uid2 = time();

        $validator = Validator::make($request->all(), [
            'first_name'=>'required',
            'second_name'=>'required',
            'phone_number'=>'required|numeric',
            'identity_no'=>'required|numeric|min:6|unique:user',
            'birth'=>'required|date',
            'birth_no'=>'required|min:6|unique:user',
            'origin_county'=>'required|numeric',
            'current_residence'=>'required|numeric',
            'occupation'=>'required'

        ]);
        if ($validator->passes()) {
            $citizen = new Citizen;
            $citizen->user_id = $uid;
            $citizen->first_name = Input::get('first_name');
            $citizen->second_name = Input::get('second_name');
            $citizen->last_name = Input::get('last_name');
            $citizen->identity_no = Input::get('identity_no');
            $citizen->birth = Input::get('birth');
            $citizen->birth_no = Input::get('birth_no');
            $citizen->origin_county_id = Input::get('origin_county');
            $citizen->county_id = Input::get('current_residence');
            $citizen->occupation = Input::get('occupation');
            $citizen->nhif_pin = Input::get('nhif_no');
            $citizen->kra_pin = Input::get('kra_pin');
            $citizen->plot_no_id = Input::get('plot_no');
            $citizen->house_no = Input::get('house_no');
            $citizen->save();


            $phone_number = new PhoneNumber;
            $phone_number->phone_number_id = $uid2;
            $phone_number->user_id = $uid;
            $phone_number->phone_no = Input::get('phone_number');
            $phone_number->save();

            return response()->json(['success' => 'New citizen saved successfully!']);
        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function show($id)
    {
        // $citizen = DB::table('user')
        //     ->leftJoin('phone_number', 'user.user_id', '=','phone_number.user_id')
        //     ->leftJoin('county', 'user.county_id', '=','county.county_id')
        //     ->select('first_name','second_name','last_name','identity_no','birth_no','kra_pin','nhif_pin','county.name as county_name','phone_number.phone_no','occupation','birth')
        //     ->where('user.user_id',$id)
        //     ->get();
        // return response()->json(['citizen_dt'=>$citizen]);
    }
    public function get_details(Request $request)
    {
        $citizen = DB::table('user')
            ->leftJoin('phone_number', 'user.user_id', '=','phone_number.user_id')
            ->leftJoin('county', 'user.county_id', '=','county.county_id')
            ->select('first_name','second_name','last_name','identity_no','birth_no','kra_pin','nhif_pin','county.name as county_name','phone_number.phone_no','occupation','birth')
            ->where('user.user_id',$request->guardian_id)
            ->get();
        return response($citizen);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:county,name,'.$id
        ]);
        if ($validator->passes()) {
            County::findOrFail($id)
            ->update([
                'name'=>$request->name
                ]);
             return response()->json(['success' => 'County updated successfully!']);

        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function destroy($id)
    {
        //
    }
    public function count()
    {
        return response(Citizen::count());
    }

    public function citizen_dependants(Request $request)
    {
        $dependants = DB::table('dependent')
            ->leftJoin('dependent_type', 'dependent.dependent_type_id', '=', 'dependent_type.dependent_type_id')
            ->leftJoin('user','dependent.dependent_user_id', '=', 'user.user_id')
            ->select('user.first_name','user.second_name','user.last_name','dependent_type.name as dependance_type')
            ->where('dependent.user_id',$request->guardian_id)
            ->get();
        return response()->json($dependants);
    }

    public function citizen_properties(Request $request)
    {
        $properties = DB::table('property')
            ->leftJoin('property_type', 'property.property_type_id', '=', 'property_type.property_type_id')
            ->select('property_type.name as property_type','property.property_description')
            ->where('property.user_id',$request->guardian_id)
            ->get();
        return response()->json($properties);
    }
    public function citizen_education(Request $request)
    {
        $educations = DB::table('education')
            ->leftJoin('school', 'education.school_id', '=', 'school.school_id')
            ->leftJoin('school_type', 'school.school_type_id', '=', 'school_type.school_type_id')
            ->select('school.name as school','school_type.name as school_type')
            ->where('education.user_id',$request->guardian_id)
            ->get();
        return response()->json($educations);
    }
    public function count_properties(Request $request)
    {
        $property_count=Property::where('property.user_id',$request->guardian_id)->count();
        return response($property_count);
    }
    public function count_dependants(Request $request)
    {
        $dependent_count=Dependent::where('dependent.user_id',$request->guardian_id)->count();
        return response($dependent_count);
    }
    
}
