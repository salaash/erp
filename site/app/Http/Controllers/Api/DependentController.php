<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Dependent;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\input;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use DB;

class DependentController extends Controller
{
    public function index()
    {
        $guardians = DB::table('dependent')
            ->leftJoin('dependent_type', 'dependent.dependent_type_id', '=', 'dependent_type.dependent_type_id')
            ->leftJoin('user','dependent.user_id', '=', 'user.user_id')
            ->select('user.first_name as guardian_fname','user.second_name as guardian_sname','user.last_name as guardian_lname','dependent_type.name as dependance_type','dependent.user_id as guardian_id','dependent.dependent_user_id as dependent_id_in_guardian')
            ->get();
        return response()->json($guardians);
    }

    public function get_dependants(Request $request)
    {
        $guardian_id = $request->guardian_id;
        $guardians = DB::table('dependent')
            ->leftJoin('user','dependent.dependent_user_id', '=', 'user.user_id')
            ->select('user.first_name as dependant_fname','user.second_name as dependant_sname','user.last_name as dependant_lname','dependent.dependent_user_id as dependent_id')
            // ->where('dependent.user_id',$guardian_id)
            ->get();
        return response()->json($guardians);
    }

    public function load_dependants()
    {
        $guardian_id = $request->guardian_id;
        $guardians = DB::table('dependent')
            ->leftJoin('user','dependent.dependent_user_id', '=', 'user.user_id')
            ->leftJoin('dependent_type', 'dependent.dependent_type_id', '=', 'dependent_type.dependent_type_id')
            ->select('user.first_name as dependant_fname','user.second_name as dependant_sname','user.last_name as dependant_lname','dependent.dependent_user_id as dependent_id','dependent_type.name as dependance_type')
            ->where('dependent.user_id',$guardian_id)
            ->get();
        return response()->json($guardians);
    }

    public function store(Request $request)
    {

        $uid = time();

        $validator = Validator::make($request->all(), [

            'guardian'=>'required',
            'dependant_type'=>'required',
            'dependant'=>'required'
        ]);
        if ($validator->passes()) {
            $dependant = new Dependent;
            $dependant->dependent_id = $uid;
            $dependant->user_id = input::get('guardian');
            $dependant->dependent_type_id = input::get('dependant_type');
            $dependant->dependent_user_id = input::get('dependant');
            $dependant->save();
            return response()->json(['success' => 'Dependant saved successfully!']);
        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function show($id)
    {

        // $property = DB::table('property')
        //     ->leftJoin('property_type', 'property.property_type_id', '=', 'property_type.property_type_id')
        //     ->leftJoin('user','property.user_id', '=', 'user.user_id')
        //     ->select('property.id','property.property_id','user.first_name','user.second_name','user.last_name','user.user_id','property_type.name','property.property_type_id','property_description')
        //     ->where('property.id',$id)
        //     ->get();
        // return response()->json($property);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'guardian'=>'required',
            'dependant_type'=>'required',
            'dependant'=>'required'
        ]);
        if ($validator->passes()) {
            Dependent::findOrFail($id)
            ->update([
                'user_id'=>$request->guardian,
                'dependent_type_id'=>$request->dependant_type,
                'dependent_user_id'=>$request->dependant
                ]);
             return response()->json(['success' => 'Dependant updated successfully!']);

        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function destroy($id)
    {
        //
    }
    public function count()
    {
        return response(Dependent::count());
    }
}
