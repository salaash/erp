<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\County;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\input;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class CountyController extends Controller
{
    public function index()
    {
        $counties = County::all();
        return response()->json($counties);
    }

    public function store(Request $request)
    {

        $uid = time();

        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:county'
        ]);
        if ($validator->passes()) {
            $county = new County;
            $county->name = Input::get('name');
            $county->county_id = $uid;
            $county->save();
            return response()->json(['success' => 'County saved successfully!']);
        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function show($id)
    {
        return response()->json(County::findOrFail($id));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:county,name,'.$id
        ]);
        if ($validator->passes()) {
            County::findOrFail($id)
            ->update([
                'name'=>$request->name
                ]);
             return response()->json(['success' => 'County updated successfully!']);

        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function destroy($id)
    {
        //
    }
}
