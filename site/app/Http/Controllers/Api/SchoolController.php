<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SchoolType;
use App\Models\School;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\input;
use Illuminate\Support\Facades\Validator;
use DB;
use Carbon\Carbon;

class SchoolController extends Controller
{
    public function index()
    {
        $schools = DB::table('school')
            ->leftJoin('school_type','school.school_type_id', '=', 'school_type.school_type_id')
            ->select('school.name','school.school_type_id','school_type.name as school_type_name','school.id','school.school_id')
            ->get();
        return response()->json($schools);
    }

    public function store(Request $request)
    {

        $uid = time();

        $validator = Validator::make($request->all(), [
            'school_type'=>'required',
            'name'=>'required|unique:school'
        ]);
        if ($validator->passes()) {
            $school = new School;
            $school->school_id = $uid;
            $school->name = input::get('name');
            $school->school_type_id = input::get('school_type');
            $school->save();
            return response()->json(['success' => 'School saved successfully!']);
        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function show($id)
    {

        $schools = DB::table('school')
            ->leftJoin('school_type', 'school.school_type_id', '=', 'school_type.school_type_id')
            ->select('school.name','school_type.name as school_type_name','school.school_type_id','school.id')
            ->where('school.id',$id)
            ->get();
        return response()->json($schools);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'school_type'=>'required',
            'name'=>'required|unique:school,name,'.$id
        ]);
        if ($validator->passes()) {
            School::findOrFail($id)
            ->update([
                'name'=>$request->name,
                'school_type_id'=>$request->school_type,
                ]);
             return response()->json(['success' => 'School updated successfully!']);

        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function destroy($id)
    {
        //
    }
    public function count()
    {
        return response(School::count());
    }
}
