<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function profile($citizen_id)
    {
        return view('profile',compact('citizen_id'));
    }
}
