<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dependent extends Model
{
    protected $table = 'dependent';
    protected $fillable = ['user_id','dependent_type_id','dependent_user_id'];
}
