<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DependentType extends Model
{
    
    protected $table = 'dependent_type';
    protected $fillable = ['dependent_type_id','name'];
}
