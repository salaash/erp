<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Citizen extends Model
{
    protected $table = 'user';
    protected $fillable = ['user_id','first_name','second_name','last_name','user_type_id','identity_no','birth_no','kra_pin','nhif_pin','county_id','plot_no_id','house_no','origin_county_id','birth','occupation'];
}
