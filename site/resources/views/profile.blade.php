@extends('layouts.main')
@section('page-css')
<link href="{{asset('plugins/light-gallery/css/lightgallery.css')}}" rel="stylesheet">
@endsection

@section('main-content')
<script type="text/javascript">
  $(document).ready(function () {
      loadDependants();
      loadProperties();
      loadEducation();
      loadDetails();

  });

</script>
<script type="text/javascript">
  function loadDetails()
  {
    var guardian_id = {{$citizen_id}};
    var formData5 = {'guardian_id':guardian_id}
    var url = base_url + "/api/get-citizen-details";
      $.post(url,formData5, function (details) {

        $("#full_name").text(details[0]['first_name']+" "+details[0]['second_name']+" "+details[0]['last_name']);
        $("#full_name1").text(details[0]['first_name']+" "+details[0]['second_name']+" "+details[0]['last_name']);
        $("#dob").text(details[0]['birth']);
        $("#cert_no").text(details[0]['birth_no']);
        $("#location").text(details[0]['county_name']);
        $("#id_no").text(details[0]['identity_no']);
        $("#kra").text(details[0]['kra_pin']);
        $("#nhif").text(details[0]['nhif_pin']);
        $("#occupation").text(details[0]['occupation']);
        $("#occupation1").text(details[0]['occupation']);
        $("#phone_no").text(details[0]['phone_no']);
    });
  }
function loadDependants()
  {
    
    var guardian_id = {{$citizen_id}};

    var formData = {'guardian_id':guardian_id}
    var url = base_url + "/api/dependants-list";
      $.post(url,formData, function (dependants) {
        var dependants_row = '';
        var no = 0;
        $.each(dependants, function(k, v) {
          no=no+1;
          dependants_row+="<tr><td>"+no+"</td><td>"+v.dependance_type+"</td><td>"+v.first_name+" "+v.second_name+" "+v.last_name+"</td></tr>";
        }); 
        $("#dependats_table").append(dependants_row);
    });

    //counting dependants
    var url3 = base_url + "/api/dependants-count";
      $.post(url3,formData, function (dependants_count) {
        $("#dependants_count").text(dependants_count);
    });
  }
function loadProperties()
{
    var guardian_id = {{$citizen_id}};

    var formData = {'guardian_id':guardian_id}
    var url = base_url + "/api/properties-list";
      $.post(url,formData, function (properties) {
        var properties_row = '';
        var no = 0;
        $.each(properties, function(k, v) {
          no=no+1;
          properties_row+="<tr><td>"+no+"</td><td>"+v.property_type+"</td><td>"+v.property_description+"</td></tr>";
        }); 
        $("#properties_table").append(properties_row);
    });

    // counting number of properties
    var url = base_url + "/api/properties-count";
      $.post(url,formData, function (properties_count) {
        $("#properties_count").text(properties_count);
    });
}
function loadEducation()
{
  var guardian_id = {{$citizen_id}};

    var formData = {'guardian_id':guardian_id}
    var url = base_url + "/api/education-list";
      $.post(url,formData, function (education) {
        var education_row = '';
        var no = 0;
        $.each(education, function(k, v) {
          no=no+1;
          education_row+="<tr><td>"+no+"</td><td>"+v.school_type+"</td><td>"+v.school+"</td></tr>";
        }); 
        $("#education_table").append(education_row);
    });
}

</script>
<!-- Responsive Table -->
<div class="row profile">
  <!-- Column -->
  <div class="col-lg-4 col-xlg-3 col-md-5">
      <div class="card">
          <div class="bg-cyan padding-30 b-t-radius">
              <div class="avatar">
                  <img src="{{asset('public/assets/images/mail/seven.jpg')}}" class="border-trans">
              </div>
          </div>
          <div class="body text-center">
                  <h4 class="card-title m-t-10" id="full_name1"></h4>
                  <h6 class="card-subtitle" id="occupation1"> </h6>
          </div>
          <div class="card-footer-bordered padding-20">
              <div class="row">
                  <div class="col-xs-6">
                      <div class="align-center">
                          <span class="font-20" id="properties_count"></span>
                          <div>Properties</div>
                      </div>
                  </div>
                  <div class="col-xs-6">
                      <div class="align-center">
                          <span class="font-20" id="dependants_count"></span>
                          <div>Dependants</div>
                      </div>
                  </div>
              </div>
          </div>
          <div>
              <hr class="m-t-0"> </div>
      </div>
  </div>
  <!-- Column -->
  <!-- Column -->
  <div class="col-lg-8 col-xlg-9 col-md-7">
      <div class="card">
          <div class="profile_body">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs nav-tabs-inline nav-justified" role="tablist">
                  <li role="presentation" class="active"><a href="#about" data-toggle="tab">ABOUT</a></li>
                  
                  <li role="presentation"><a href="#education" data-toggle="tab">EDUCATION</a></li>
                  <li role="presentation"><a href="#dependants" data-toggle="tab">DEPENDANTS</a></li>
                  <li role="presentation"><a href="#properties" data-toggle="tab">PROPERTIES</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                  <div role="tabpanel" class="tab-pane fade in active" id="about">
                      <div class="card-inner">
                          <h2 class="card-inner-header"><i class="fa fa-address-book m-r-5"></i> Basic Information</h2>
                          <div class="demo">
                              <div class="p-l-25">
                                  <dl class="dl-horizontal">
                                      <dt>Full Name</dt>
                                      <dd id="full_name"></dd>
                                  </dl>
                                  <dl class="dl-horizontal">
                                      <dt>Mobile Number</dt>
                                      <dd id="phone_no"></dd>
                                  </dl>
                                  <dl class="dl-horizontal">
                                      <dt>ID Number</dt>
                                      <dd id="id_no"></dd>
                                  </dl>
                                  <dl class="dl-horizontal">
                                      <dt>Location</dt>
                                      <dd id="location"></dd>
                                  </dl>
                                  <dl class="dl-horizontal">
                                    <dt>Date of Birth</dt>
                                    <dd id="dob">29 years</dd>
                                  </dl>
                                  <dl class="dl-horizontal">
                                    <dt>Birth Cert Number</dt>
                                    <dd id="cert_no"></dd>
                                  </dl>
                                  
                              </div>
                          </div>
                      </div>
                      <div class="card-inner">
                          <h2 class="card-inner-header"><i class="fa fa-user m-r-5"></i> Employment Info</h2>
                          <div class="demo">
                              <div class="p-l-25">
                                  <dl class="dl-horizontal">
                                      <dt>Occupation</dt>
                                      <dd id="occupation"></dd>
                                  </dl>
                                  <dl class="dl-horizontal">
                                      <dt>KRA PIN</dt>
                                      <dd id="kra"></dd>
                                  </dl>
                                  <dl class="dl-horizontal">
                                      <dt>NHIF PIN</dt>
                                      <dd id="nhif"></dd>
                                  </dl>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="properties">
                      <!-- Striped Rows -->
                      <table class="table table-striped" id="properties_table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Property Type</th>
                            <th>Description</th>
                        </tr>
                        </thead>

                    </table>
                    <!-- #END# Striped Rows -->
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="dependants">
                      <!-- Striped Rows -->
                      <table class="table table-striped" id="dependats_table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Dependant Type</th>
                            <th>Dependant</th>
                        </tr>
                        </thead>
                    </table>
                    <!-- #END# Striped Rows -->
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="education">
                      <!-- Striped Rows -->
                      <table class="table table-striped" id="education_table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>School Type</th>
                            <th>School</th>
                        </tr>
                        </thead>
                    </table>
                    <!-- #END# Striped Rows -->
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- Column -->
</div>
    <!-- #END# RESPONSIVE Table -->
<script type="text/javascript">
    function newCitizen()
    {
        document.getElementById("new_citizen_form").reset();
        $('.error-class').empty();
        $('#addModal').modal('show');
    }
    function EditModal(id)
    {
        document.getElementById("edit_form").reset();
        $('.error-class').empty();
        var url = base_url + "/api/citizens/"+id;
        $.get(url, function (member) {
          $('#edit_form input[id=citizen_id]').val(member['id']);
          $('#edit_form input[id=name]').val(member['name']);

          $('#editModal').modal('show');
      });
    }
    

    
</script>

@endsection

@section('additional-scripts')
{{-- <script src="{{asset('public/plugins/light-gallery/js/lightgallery-all.js')}}"></script>
<script src="{{asset('public/assets/js/pages/medias/image-gallery.js')}}"></script> --}}
@endsection