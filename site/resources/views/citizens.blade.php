@extends('layouts.main')
{{-- @section('breadcrump')
<div class="page-header">
    <h2>Citizens</h2>
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="active">Citizens</li>
    </ol>
</div>
@endsection --}}
@section('main-content')
<script type="text/javascript">
  $(document).ready(function () {
      loadCitizens();

    var url = base_url + "/api/counties";
    var county_of_origin = " ";
      $.getJSON(url, function (banks) {
        county_of_origin+="<option value=''>"+'Select county'+"</option>";
        $.each(banks,function(index,item) 
        {
          county_of_origin+="<option value='"+item.county_id+"'>"+item.name+"</option>";
        });
        $("#origin_county").html(county_of_origin);
      });  

      var current_residence = " ";
      $.getJSON(url, function (banks) {
        current_residence+="<option value=''>"+'Select county'+"</option>";
        $.each(banks,function(index,item) 
        {
          current_residence+="<option value='"+item.county_id+"'>"+item.name+"</option>";
        });
        $("#current_residence").html(current_residence);
      }); 
  });

</script>
<script type="text/javascript">
function loadCitizens()
  {
    // var url = base_url + "/group-details";property-types
    var url = '{{url('api/citizens')}}';
    $.ajax({
      type:'GET',
      url:url,
      success: function(citizens)
      {

        var citizens_row = '';
        var no = 0;
        $('#citizens_table').dataTable().fnClearTable();
        $('#citizens_table').dataTable().fnDestroy();
        $.each(citizens, function(k, v) {
        var url = '{{ route("citizen-profile",["citizen_id" => 'id_here']) }}';
        url = url.replace('id_here', v.user_id);


          no=no+1;
          citizens_row+="<tr><td>"+no+"</td><td>"+v.first_name+" "+v.second_name+" "+v.last_name+"</td><td>"+v.identity_no+"</td><td>"+v.phone_no+"</td><td>"+v.occupation+"</td><td>"+v.county_id+"</td><td>"+v.birth+"</td><td><div class='btn-group'><button type='button' class='btn btn-success dropdown-toggle btn-xs' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Actions<span class='caret'></span></button>\n\
              <div class='dropdown-menu'>\n\
                  <ul style='list-style-type:none'>\n\
                  <li><a class='dropdown-item' href='"+url+"'>Profile</a></li>\n\
                  </ul>\n\
              </div></div></td></tr>";

             
        }); 
        $("#citizens_table").append(citizens_row);
        $('#citizens_table').DataTable({
          info:true,
          searching:true,
          bFilter:true,
          lengthChange:true
        });
      }
    });
  }

</script>
<!-- Responsive Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-sm btn-success pull-right" href="javascript: newCitizen()">Add new citizen</a>                   
                </div>
                <div class="body">
                    <table id="citizens_table" class="table table-striped table-bordered dt-responsive nowrap dataTable" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Names</th>
                            <th>ID Number</th>
                            <th>Phone Number</th>
                            <th>Occupation</th>
                            <th>Residence</th>
                            <th>DOB</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# RESPONSIVE Table -->
<script type="text/javascript">
    function newCitizen()
    {
        document.getElementById("new_citizen_form").reset();
        $('.error-class').empty();
        $('#addModal').modal('show');
    }
    function EditModal(id)
    {
        document.getElementById("edit_form").reset();
        $('.error-class').empty();
        var url = base_url + "/api/citizens/"+id;
        $.get(url, function (member) {
          $('#edit_form input[id=citizen_id]').val(member['id']);
          $('#edit_form input[id=name]').val(member['name']);

          $('#editModal').modal('show');
      });
    }
    

    
</script>

@endsection

@section('modals')
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">New Citizen Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'new_citizen_form']) !!}
                @include('forms.citizen')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="savebtn">Save citizen Type</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit citizen Type Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'edit_form']) !!}
            <input type="hidden" name="citizen_id" id="citizen_id">
                @include('forms.user_type')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="updatebtn">Update citizen Type</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#savebtn').click(function(){
    var form = $("#new_citizen_form");
    var formData = form.serialize();
    $('.error-class').empty();
    $.ajax({
      url:base_url + "/api/citizens",
      type:'POST',
      data:formData,
      success:function(dat) {
        var error_values = JSON.stringify(dat);
        var data = JSON.parse(error_values);
        // console.log(data);
          if(data['errors']) {
            
            $( '#new_citizen_form [id=first_name_error]' ).html(data['errors']['first_name']);
            $( '#new_citizen_form [id=second_name_error]' ).html(data['errors']['second_name']);
            $( '#new_citizen_form [id=last_name_error]' ).html(data['errors']['last_name']);
            $( '#new_citizen_form [id=id_no_error]' ).html(data['errors']['identity_no']);
            $( '#new_citizen_form [id=phone_number_error]' ).html(data['errors']['phone_number']);
            $( '#new_citizen_form [id=birth_error]' ).html(data['errors']['birth']);
            $( '#new_citizen_form [id=birth_no_error]' ).html(data['errors']['birth_no']);
            $( '#new_citizen_form [id=origin_county_error]' ).html(data['errors']['origin_county']);
            $( '#new_citizen_form [id=current_residence_error]' ).html(data['errors']['current_residence']);
            $( '#new_citizen_form [id=occupation_error]' ).html(data['errors']['occupation']);
          }
          else {
            $('#addModal').modal('hide');
            loadCitizens();
            new PNotify({
                text: data['success'],
                type: 'success'
            });
          }
      },
    });
    });
    $('#updatebtn').click(function () {
      var editform = $("#edit_form");
      var formData = editform.serialize();
      var id = document.getElementById("citizen_id").value;
      $('.error-class').empty();
      var url = base_url + "/api/user-types/"+id;
      $.ajax({
          url:url,
          type:'put',
          data:formData,
          success:function(data) {
            // var error_values = JSON.parse(data);
              if(data['errors']) {
               $( '#edit_form [id=name_error]' ).html(data['errors']['name'][0]);

              }
              else {
                $('#editModal').modal('hide');
                loadCitizens();
                new PNotify({
                    text: data['success'],
                    type: 'success'
                });
              }
          },
      });
    });
</script>
@endsection