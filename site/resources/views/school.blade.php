@extends('layouts.main')

@section('main-content')
<script type="text/javascript">
  $(document).ready(function () {
      loadSchools(); 

    var url = base_url + "/api/school-types";
    var school_type = " ";
      $.getJSON(url, function (school_types) {
        // console.log(school_types);
        school_type+="<option value=''>"+'Select school type'+"</option>";
        $.each(school_types,function(index,item) 
        {
          school_type+="<option value='"+item.school_type_id+"'>"+item.name+"</option>";
        });
        $("#school_type").html(school_type);
      });  

      // populating select fields for edit
      var edit_school_type = " ";
      $.getJSON(url, function (edit_school) {
        edit_school_type+="<option value=''>"+'Select school type'+"</option>";
        $.each(edit_school,function(index,item) 
        {
          edit_school_type+="<option value='"+item.school_type_id+"'>"+item.name+"</option>";
        });
        $("#edit_form select[id=school_type]").html(edit_school_type);
      });

  });

</script>
<script type="text/javascript">
function loadSchools()
  {
    // var url = base_url + "/group-details";property-types
    var url = '{{url('api/schools')}}';
    $.ajax({
      type:'GET',
      url:url,
      success: function(schools)
      {
        // console.log(schools);
        // console.log(JSON.stringify(counties));
        var school_rows = '';
        var no = 0;
        $('#schools_table').dataTable().fnClearTable();
        $('#schools_table').dataTable().fnDestroy();
        $.each(schools, function(k, v) {
          no=no+1;
          school_rows+="<tr><td>"+no+"</td><td>"+v.name+"</td><td>"+v.school_type_name+"</td><td><div class='btn-group'><button type='button' class='btn btn-success dropdown-toggle btn-xs' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Actions Buttons <span class='caret'></span></button>\n\
              <div class='dropdown-menu'>\n\
                  <ul style='list-style-type:none'><li><a class='dropdown-item' href='javascript:EditModalfn("+v.id+")'>Edit</a></li></ul>\n\
              </div></div></td></tr>";

             
        }); 
        $("#schools_table").append(school_rows);
        $('#schools_table').DataTable({
          info:true,
          searching:false,
          bFilter:false,
          lengthChange:false
        });
      }
    });
  }

</script>
<!-- Responsive Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-sm btn-success pull-right" href="javascript: newSchool()">Add school</a>                   
                </div>
                <div class="body">
                    <table id="schools_table" class="table table-striped table-bordered dt-responsive nowrap dataTable" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>School</th>
                            <th>School Type</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# RESPONSIVE Table -->
@endsection

@section('modals')
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">New school Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'new_form']) !!}
                @include('forms.school')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="savebtn">Save School</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit School Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'edit_form']) !!}
            <input type="hidden" name="school_id" id="school_id">
                @include('forms.school')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="updatebtn">Update school</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function newSchool()
    {
        document.getElementById("new_form").reset();
        // document.getElementById("savebtn").disabled = false;
        $('.error-class').empty();
        $('#addModal').modal('show');
    }
    function EditModalfn(id)
    {
      // alert(id);
        document.getElementById("edit_form").reset();
        // document.getElementById("updatebtn").disabled = false;
        $('.error-class').empty();
        var url = base_url + "/api/schools/"+id;
        $.get(url, function (school) {
          var data = JSON.stringify(school);
          var schooldata = JSON.parse(data);
          $('#edit_form input[id=school_id]').val(school[0].id);

          $('#edit_form input[id=name]').val(school[0].name);
          $('#edit_form select[id=school_type]').val(school[0].school_type_id).change();

          $('#editModal').modal('show');
      });
    }


    $('#savebtn').click(function(){
    var form = $("#new_form");
    var formData = form.serialize();
    $('.error-class').empty();
    // document.getElementById("savebtn").disabled = true;
    $.ajax({
      url:base_url + "/api/schools",
      type:'POST',
      data:formData,
      success:function(dat) {
        var error_values = JSON.stringify(dat);
        var data = JSON.parse(error_values);
          if(data['errors']) {
            $( '#new_form [id=name_error]' ).html(data['errors']['name']);
            $( '#new_form [id=school_type_error]' ).html(data['errors']['school_type']);
          }
          else {
            $('#addModal').modal('hide');
            loadSchools();
            new PNotify({
                text: data['success'],
                type: 'success'
            });
          }
      },
    });
    });
    $('#updatebtn').click(function () {
      var editform = $("#edit_form");
      var formData = editform.serialize();
      var id = document.getElementById("school_id").value;
      // document.getElementById("updatebtn").disabled = true;
      $('.error-class').empty();
      var url = base_url + "/api/schools/"+id;
      $.ajax({
          url:url,
          type:'put',
          data:formData,
          success:function(dat) {
            var error_values = JSON.stringify(dat);
            var data = JSON.parse(error_values);
              if(data['errors']) {
                $( '#new_form [id=name_error]' ).html(data['errors']['name']);
                $( '#new_form [id=school_type_error]' ).html(data['errors']['school_type']);

              }
              else {
                $('#editModal').modal('hide');
                loadSchools();
                new PNotify({
                    text: data['success'],
                    type: 'success'
                });
              }
          },
      });
    });
</script>
@endsection