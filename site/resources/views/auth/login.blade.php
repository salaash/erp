@extends('layouts.authentication')
@section('content')
<div class="login-box">
    <div class="card">
        <div class="body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="login-logo">
                        <a  href="{{ url('/') }}">
                            <p>{{ config('app.name', 'login') }}</p>
                        </a>
                    </div>
                </div>
            </div>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="input-group addon-line">
                    <span class="input-group-addon">
                        <i class="material-icons">person</i>
                    </span>
                    <div class="form-line">
                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" id="email" placeholder="Email" required autofocus>
                                                   
                    </div>
                    @if ($errors->has('email'))
                        <span class="" role="alert" style="color: red">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif 
                </div>
                <div class="input-group addon-line">
                    <span class="input-group-addon">
                        <i class="material-icons">lock</i>
                    </span>
                    <div class="form-line">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 p-t-5">
                        <input class="filled-in chk-col-blue" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label for="rememberme">Remember Me</label>
                    </div>
                    <div class="col-xs-6 align-right p-t-5">
                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                Forgot Password?
                            </a>
                        @endif
                        
                    </div>
                </div>
                <button type="submit" class="btn btn-block btn-primary waves-effect" ><span style="color:#ffffff">LOG IN</span></button>

                <p class="text-muted text-center p-t-20">
                    <small>Do not have an account?</small>
                </p>

                <a class="btn btn-sm btn-default btn-block" href="{{ route('register') }}">Create an account</a>

            </form>
        </div>
    </div>
</div>

@endsection
