@extends('layouts.main')
@section('breadcrump')
{{-- <div class="page-header"> --}}
    {{-- <a class="btn btn-sm btn-success">Add County</a> --}}
{{--     <ol class="breadcrumb">
        <li><i class="fa fa-home"> </i> <a href="{{url('/')}}">Home</a></li>
        <li class="active">Counties</li>
    </ol> --}}
{{-- </div> --}}
@endsection
@section('main-content')
<script type="text/javascript">
  $(document).ready(function () {
      loadcounties();  
  });

</script>
<script type="text/javascript">
function loadcounties()
  {
    var url = '{{url('api/counties')}}';
    $.ajax({
      type:'GET',
      url:url,
      success: function(counties)
      {
        var counties_row = '';
        var no = 0;
        $('#counties_table').dataTable().fnClearTable();
        $('#counties_table').dataTable().fnDestroy();
        $.each(counties, function(k, v) {
          no=no+1;
          counties_row+="<tr><td>"+no+"</td><td>"+v.name+"</td><td><div class='btn-group'><button type='button' class='btn btn-success dropdown-toggle btn-xs' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Actions Buttons <span class='caret'></span></button>\n\
              <div class='dropdown-menu'>\n\
                  <ul style='list-style-type:none'><li><a class='dropdown-item' href='javascript:EditCountyModalfn("+v.id+")'>Edit</a></li></ul>\n\
              </div></div></td></tr>";

             
        }); 
        $("#counties_table").append(counties_row);
        $('#counties_table').DataTable({
          info:true,
          searching:false,
          bFilter:false,
          lengthChange:false
        });
      }
    });
  }

</script>
<!-- Responsive Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-sm btn-success pull-right" href="javascript: new_county()">Add county</a>                   
                </div>
                <div class="body">
                    <table id="counties_table" class="table table-striped table-bordered dt-responsive nowrap dataTable" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# RESPONSIVE Table -->
<script type="text/javascript">
    function new_county()
    {
        document.getElementById("new_county_form").reset();
        $('.error-class').empty();
        $('#newcountymodal').modal('show');
    }
    function EditCountyModalfn(id)
    {
        document.getElementById("edit_county_form").reset();
        $('.error-class').empty();
        var url = base_url + "/api/counties/"+id;
        $.get(url, function (member) {
          $('#edit_county_form input[id=county_id]').val(member['id']);
          $('#edit_county_form input[id=name]').val(member['name']);

          $('#editcountymodal').modal('show');
      });
    }
    

    
</script>

@endsection

@section('modals')
<div class="modal fade" id="newcountymodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">New County Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'new_county_form']) !!}
                @include('forms.county')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save_county_btn">Save County</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editcountymodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit County Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'edit_county_form']) !!}
            <input type="hidden" name="county_id" id="county_id">
                @include('forms.county')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="updatecountybtn">Update County</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#save_county_btn').click(function(){
    var newcountyform = $("#new_county_form");
    var formData = newcountyform.serialize();
    $('.error-class').empty();
    $.ajax({
      url:base_url + "/api/counties",
      type:'POST',
      data:formData,
      success:function(data) {
        console.log(data);
          if(data['errors']) {
            $( '#new_county_form [id=name_error]' ).html(data['errors']['name'][0]);
          }
          else {
            $('#newcountymodal').modal('hide');
            loadcounties();
            new PNotify({
                text: data['success'],
                type: 'success'
            });
          }
      },
    });
    });
    $('#updatecountybtn').click(function () {
      var editcountyform = $("#edit_county_form");
      var formData = editcountyform.serialize();
      var id = document.getElementById("county_id").value;
      $('.error-class').empty();
      var url = base_url + "/api/counties/"+id;
      $.ajax({
          url:url,
          type:'put',
          data:formData,
          success:function(data) {
            // var error_values = JSON.parse(data);
              if(data['errors']) {
               $( '#edit_county_form [id=name_error]' ).html(data['errors']['name'][0]);

              }
              else {
                $('#editcountymodal').modal('hide');
                loadcounties();
                new PNotify({
                    text: data['success'],
                    type: 'success'
                });
              }
          },
      });
    });
</script>
@endsection