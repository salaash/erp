<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>{{ config('app.name') }}</title>
    <!-- Favicon-->
    <link rel="icon" href="{{ asset('favicon.png') }}" type="image/x-icon">

    <!--REQUIRED PLUGIN CSS-->
    <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/node-waves/waves.css') }}" rel="stylesheet" />
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />
    <link href="{{ asset('plugins/spinkit/spinkit.css') }}" rel="stylesheet">

    <!--THIS PAGE LEVEL CSS-->
    <link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/nifty-modal/component.css')}}" rel="stylesheet" />
    <link href="{{asset('pnotify/pnotify.custom.min.css')}}" rel="stylesheet" type="text/css">

    @yield('page-css')
    <!--REQUIRED THEME CSS -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/layout.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/main_theme.css') }}" rel="stylesheet" />

    {{-- jquery --}}
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        table.dataTable th.focus, table.dataTable td.focus {
            outline: 2px solid #ee8d8d !important;
            outline-offset: -1px;
        }
    </style>
</head>

<body class="theme-indigo dark layout-fixed">
<div class="wrapper">
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="sk-wave">
                <div class="sk-rect sk-rect1 bg-cyan"></div>
                <div class="sk-rect sk-rect2 bg-cyan"></div>
                <div class="sk-rect sk-rect3 bg-cyan"></div>
                <div class="sk-rect sk-rect4 bg-cyan"></div>
                <div class="sk-rect sk-rect5 bg-cyan"></div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- top navbar-->
    <header class="topnavbar-wrapper">
        <nav role="navigation" class="navbar topnavbar">
            <!-- START navbar header-->
            <div class="navbar-header">
                <a href="{{url('/')}}" class="navbar-brand">
                    <div class="brand-logo">
                        {{-- <img src="{{asset('public/assets/images/logo.png')}}" alt="Admin Logo" class="img-responsive"> --}}
                        <h4 style="color: white;">ERP</h4>
                    </div>
                    <div class="brand-logo-collapsed">
                        <img src="{{asset('assets/images/logo-single.png')}}" alt="Admin Logo" class="img-responsive">
                    </div>
                </a>
            </div>
            <!-- END navbar header-->
            <!-- START Nav wrapper-->
            <div class="nav-wrapper">
                <!-- START Left navbar-->
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#" data-trigger-resize="" data-toggle-state="aside-collapsed" class="hidden-xs">
                            <em class="material-icons">menu</em>
                        </a>
                        <a href="#" data-toggle-state="aside-toggled" data-no-persist="true" class="visible-xs sidebar-toggle">
                            <em class="material-icons">menu</em>
                        </a>
                    </li>
                </ul>
                <!-- END Left navbar-->
            </div>
            <!-- #END# Nav wrapper-->

        </nav>
        <!-- END Top Navbar-->
    </header>
    <!-- sidebar-->
    <aside class="aside">
        <!-- START Sidebar (left)-->
        <div class="aside-inner">
            <nav data-sidebar-anyclick-close="" class="sidebar">
                <!-- START sidebar nav-->
                <ul class="nav menu">
                    <!-- Iterates over all sidebar items-->
                    
                    <li class="nav-heading ">
                        <span>{{ Auth::user()->name }}</span>
                        <br/>
                        <span>MAIN NAVIGATION</span>
                    </li>
                    <li>
                        <a href="{{route('home')}}" title="Dashboard">
                            <em class="material-icons">home</em>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('citizens')}}" title="citizens">
                            <em class="material-icons">perm_identity</em>
                            <span>Citizens</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('dependant-view')}}" title="citizens">
                            <em class="material-icons">group</em>
                            <span>Dependants</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('education-view')}}" title="education">
                            <em class="material-icons">schools</em>
                            <span>Education</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('properties-view')}}" title="properties">
                            <em class="material-icons">account_balance</em>
                            <span>Properties</span>
                        </a>
                    </li>
                    <li>
                        <a href="#appview" title="App Views" data-toggle="collapse" class="menu-toggle">
                            <em class="material-icons">settings</em>
                            <span>System Configuration</span>
                        </a>
                        <ul id="appview" class="nav sidebar-subnav collapse">
                            <li class="sidebar-subnav-header">System Configuration</li>
                            <li>
                                <a href="{{ url('counties-view') }}" title="counties">
                                    <span>Counties</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('dependent-type') }}" title="Dependent types">
                                    <span>Dependent type</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('property-type') }}" title="Property types">
                                    <span>Property type</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('school-type-view') }}" title="school type">
                                    <span>School type</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('schools-view') }}" title="school">
                                    <span>Schools</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('user-type') }}" title="User types">
                                    <span>User types</span>
                                </a>
                            </li>
                            
                        </ul>
                    </li>


                    <li>
                        <a href="{{ route('logout') }}" title="properties">
                            
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <em class="material-icons">exit_to_app</em>
                            <span>Logout</span></a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                        </a>


{{-- <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
    <a class="dropdown-item" href="{{ route('logout') }}"
       onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div> --}}





                    </li>
                </ul>
                <!-- END sidebar nav-->
            </nav>
        </div>
        <!-- #END# Sidebar (left)-->
    </aside>

    <!-- Main section-->
    <section>
        <!-- Page content-->
        <div class="content-wrapper">
            <div class="container-fluid">
                
                @yield('breadcrump')

                @yield('main-content')
            </div>
        </div>
    </section>
    <!-- FOOTER-->
    
</div>
<!-- CORE PLUGIN JS -->
<script type="text/javascript">var base_url = '{{url("/")}}';</script>
{{-- pnotify --}}
<script src="{{asset('pnotify/pnotify.custom.min.js')}}"></script>

<script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('plugins/modernizr/modernizr.custom.js') }}"></script>
<script src="{{ asset('plugins/screenfull/dist/screenfull.js') }}"></script>
<script src="{{ asset('plugins/jQuery-Storage-API/jquery.storageapi.js') }}"></script>
<script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

<!--THIS PAGE LEVEL JS-->
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.fixedHeader.min.js') }}"></script>

{{-- modals --}}
<script src="{{ asset('assets/js/pages/ui/modals.js')}}"></script>

@yield('additional-scripts')

{{-- <script src="{{ asset('public/assets/js/pages/tables/jquery-datatable.js') }}"></script> --}}
<script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}"></script>
{{-- @yield('scripts') --}}
<!-- LAYOUT JS -->
<script src="{{ asset('assets/js/demo.js') }}"></script>
<script src="{{ asset('assets/js/layout.js') }}"></script>

</body>
@yield('modals')

</html>