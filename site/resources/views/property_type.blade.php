@extends('layouts.main')
{{-- @section('breadcrump')
<div class="page-header">
    <h2>Property type</h2>
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="active">Property type</li>
    </ol>
</div>
@endsection --}}
@section('main-content')
<script type="text/javascript">
  $(document).ready(function () {
      loadPropertyTypes();  
  });

</script>
<script type="text/javascript">
function loadPropertyTypes()
  {
    // var url = base_url + "/group-details";property-types
    var url = '{{url('api/property-types')}}';
    $.ajax({
      type:'GET',
      url:url,
      success: function(property_types)
      {

        // console.log(JSON.stringify(counties));
        var property_types_row = '';
        var no = 0;
        $('#property_types_table').dataTable().fnClearTable();
        $('#property_types_table').dataTable().fnDestroy();
        $.each(property_types, function(k, v) {
          no=no+1;
          property_types_row+="<tr><td>"+no+"</td><td>"+v.name+"</td><td><div class='btn-group'><button type='button' class='btn btn-success dropdown-toggle btn-xs' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Actions Buttons <span class='caret'></span></button>\n\
              <div class='dropdown-menu'>\n\
                  <ul style='list-style-type:none'><li><a class='dropdown-item' href='javascript:EditModal("+v.id+")'>Edit</a></li></ul>\n\
              </div></div></td></tr>";

             
        }); 
        $("#property_types_table").append(property_types_row);
        $('#property_types_table').DataTable({
          info:true,
          searching:false,
          bFilter:false,
          lengthChange:false
        });
      }
    });
  }

</script>
<!-- Responsive Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-sm btn-success pull-right" href="javascript: newPropertyType()">Add property type</a>                   
                </div>
                <div class="body">
                    <table id="property_types_table" class="table table-striped table-bordered dt-responsive nowrap dataTable" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# RESPONSIVE Table -->
<script type="text/javascript">
    function newPropertyType()
    {
        document.getElementById("new_property_type_form").reset();
        $('.error-class').empty();
        $('#newPropertyTypeModal').modal('show');
    }
    function EditModal(id)
    {
        document.getElementById("edit_property_type_form").reset();
        $('.error-class').empty();
        var url = base_url + "/api/property-types/"+id;
        $.get(url, function (member) {
          $('#edit_property_type_form input[id=property_type_id]').val(member['id']);
          $('#edit_property_type_form input[id=name]').val(member['name']);

          $('#editPropertyTypeModal').modal('show');
      });
    }
    

    
</script>

@endsection

@section('modals')
<div class="modal fade" id="newPropertyTypeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">New Property Type Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'new_property_type_form']) !!}
                @include('forms.property_type')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="savePropertyTypeBtn">Save Property Type</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editPropertyTypeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Property Type Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'edit_property_type_form']) !!}
            <input type="hidden" name="property_type_id" id="property_type_id">
                @include('forms.property_type')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="updatePropertyTypeBtn">Update Property Type</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#savePropertyTypeBtn').click(function(){
    var form = $("#new_property_type_form");
    var formData = form.serialize();
    $('.error-class').empty();
    $.ajax({
      url:base_url + "/api/property-types",
      type:'POST',
      data:formData,
      success:function(data) {
          if(data['errors']) {
            $( '#new_property_type_form [id=name_error]' ).html(data['errors']['name'][0]);
          }
          else {
            $('#newPropertyTypeModal').modal('hide');
            loadPropertyTypes();
            new PNotify({
                text: data['success'],
                type: 'success'
            });
          }
      },
    });
    });
    $('#updatePropertyTypeBtn').click(function () {
      var editform = $("#edit_property_type_form");
      var formData = editform.serialize();
      var id = document.getElementById("property_type_id").value;
      $('.error-class').empty();
      var url = base_url + "/api/property-types/"+id;
      $.ajax({
          url:url,
          type:'put',
          data:formData,
          success:function(data) {
            // var error_values = JSON.parse(data);
              if(data['errors']) {
               $( '#edit_property_type_form [id=name_error]' ).html(data['errors']['name'][0]);

              }
              else {
                $('#editPropertyTypeModal').modal('hide');
                loadPropertyTypes();
                new PNotify({
                    text: data['success'],
                    type: 'success'
                });
              }
          },
      });
    });
</script>
@endsection