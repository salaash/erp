@extends('layouts.main')
{{-- @section('breadcrump')
<div class="page-header">
    <h2>School type</h2>
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="active">School type</li>
    </ol>
</div>
@endsection --}}
@section('main-content')
<script type="text/javascript">
  $(document).ready(function () {
      loadSchoolTypes();  
  });

</script>
<script type="text/javascript">
function loadSchoolTypes()
  {
    // var url = base_url + "/group-details";property-types
    var url = '{{url('api/school-types')}}';
    $.ajax({
      type:'GET',
      url:url,
      success: function(school_types)
      {
        var school_types_row = '';
        var no = 0;
        $('#school_types_table').dataTable().fnClearTable();
        $('#school_types_table').dataTable().fnDestroy();
        $.each(school_types, function(k, v) {
          no=no+1;
          school_types_row+="<tr><td>"+no+"</td><td>"+v.name+"</td><td><div class='btn-group'><button type='button' class='btn btn-success dropdown-toggle btn-xs' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Actions Buttons <span class='caret'></span></button>\n\
              <div class='dropdown-menu'>\n\
                  <ul style='list-style-type:none'><li><a class='dropdown-item' href='javascript:EditModal("+v.id+")'>Edit</a></li></ul>\n\
              </div></div></td></tr>";

             
        }); 
        $("#school_types_table").append(school_types_row);
        $('#school_types_table').DataTable({
          info:true,
          searching:false,
          bFilter:false,
          lengthChange:false
        });
      }
    });
  }

</script>
<!-- Responsive Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-sm btn-success pull-right" href="javascript: newSchoolType()">Add school type</a>                   
                </div>
                <div class="body">
                    <table id="school_types_table" class="table table-striped table-bordered dt-responsive nowrap dataTable" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# RESPONSIVE Table -->
<script type="text/javascript">
    function newSchoolType()
    {
        document.getElementById("new_form").reset();
        $('.error-class').empty();
        $('#newModal').modal('show');
    }
    function EditModal(id)
    {
        document.getElementById("edit_form").reset();
        $('.error-class').empty();
        var url = base_url + "/api/school-types/"+id;
        $.get(url, function (school_type) {
          $('#edit_modal input[id=school_type_id]').val(school_type['id']);
          $('#edit_modal input[id=name]').val(school_type['name']);

          $('#edit_modal').modal('show');
      });
    }
    

    
</script>

@endsection

@section('modals')
<div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">New School Type Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'new_form']) !!}
                @include('forms.school_type')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveBtn">Save School Type</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit School Type Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'edit_form']) !!}
            <input type="hidden" name="school_type_id" id="school_type_id">
                @include('forms.school_type')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="updateBtn">Update School Type</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#saveBtn').click(function(){
    var form = $("#new_form");
    var formData = form.serialize();
    $('.error-class').empty();
    $.ajax({
      url:base_url + "/api/school-types",
      type:'POST',
      data:formData,
      success:function(data) {
          if(data['errors']) {
            $( '#new_form [id=name_error]' ).html(data['errors']['name'][0]);
          }
          else {
            $('#newModal').modal('hide');
            loadSchoolTypes();
            new PNotify({
                text: data['success'],
                type: 'success'
            });
          }
      },
    });
    });
    $('#updateBtn').click(function () {
      var editform = $("#edit_form");
      var formData = editform.serialize();
      var id = document.getElementById("school_type_id").value;
      $('.error-class').empty();
      var url = base_url + "/api/school-types/"+id;
      $.ajax({
          url:url,
          type:'put',
          data:formData,
          success:function(data) {
              if(data['errors']) {
               $( '#edit_modal [id=name_error]' ).html(data['errors']['name'][0]);
              }
              else {
                $('#edit_modal').modal('hide');
                loadSchoolTypes();
                new PNotify({
                    text: data['success'],
                    type: 'success'
                });
              }
          },
      });
    });
</script>
@endsection