@extends('layouts.main')
@section('main-content')
<script type="text/javascript">
  $(document).ready(function () {
    loadguardians();
      loaddependants();
      

    var url = base_url + "/api/citizens";
    var guardian_row = " ";
      $.getJSON(url, function (banks) {
        guardian_row+="<option value=''>"+'Select guardian'+"</option>";
        $.each(banks,function(index,item) 
        {
          guardian_row+="<option value='"+item.user_id+"'>"+item.first_name+" "+item.second_name+" "+item.last_name+"</option>";
        });
        $("#guardian").html(guardian_row);
      });  

      var dependat_row = " ";
      $.getJSON(url, function (dependants) {
        dependat_row+="<option value=''>"+'Select dependant'+"</option>";
        $.each(dependants,function(index,item) 
        {
          dependat_row+="<option value='"+item.user_id+"'>"+item.first_name+" "+item.second_name+" "+item.last_name+"</option>";
        });
        $("#dependant").html(dependat_row);
      }); 

      var url = base_url + "/api/dependent-types";
      var dependant_type_row = " ";
      $.getJSON(url, function (dependant_types) {
        dependant_type_row+="<option value=''>"+'Select dependant type'+"</option>";
        $.each(dependant_types,function(index,item) 
        {
          dependant_type_row+="<option value='"+item.dependent_type_id+"'>"+item.name+"</option>";
        });
        $("#dependant_type").html(dependant_type_row);
      }); 
  });

</script>
<script type="text/javascript">
  var findDependantArray=[];
  function loadguardians()
  {

    
    $.ajax({
      url:base_url + "/api/get-dependants",
      type:'POST',
      success:function(mData) {
       // console.log(mData);
          $.each(mData, function(key, value) {

            findDependantArray[value.dependent_id] = value.dependant_fname+" "+value.dependant_sname+" "+value.dependant_lname;

          });
        

     },
    });
  }
function loaddependants()
  {
    // var url = base_url + "/group-details";property-types
    var url = '{{url('api/dependants')}}';
    $.ajax({
      type:'GET',
      url:url,
      success: function(dependants)
      {
        var dependants_rows = '';
        var no = 0;
        $('#dependants_table').dataTable().fnClearTable();
        $('#dependants_table').dataTable().fnDestroy();
        $.each(dependants, function(k, v) {

          no=no+1;
          dependants_rows+="<tr><td>"+no+"</td><td>"+v.guardian_fname+" "+v.guardian_sname+" "+v.guardian_lname+"</td><td>"+v.dependance_type+"</td>";

          dependants_rows += "<td>"+findDependantArray[v.dependent_id_in_guardian]+"</td>";



          // dependants_rows+="<td><ul>"+'<li>v.guardian_fname</li><li>v.guardian_fname</li>'+"</ul></td>";
          dependants_rows+="<td><div class='btn-group'><button type='button' class='btn btn-success dropdown-toggle btn-xs' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Actions<span class='caret'></span></button>\n\
              <div class='dropdown-menu'>\n\
                  <ul style='list-style-type:none'><li><a class='dropdown-item' href='javascript:EditModal("+v.id+")'>Edit</a></li>\n\
                  </ul>\n\
              </div></div></td></tr>";

             
        }); 
        $("#dependants_table").append(dependants_rows);
        $('#dependants_table').DataTable({
          info:true,
          searching:true,
          bFilter:true,
          lengthChange:true
        });
      }
    });
  }

</script>
<!-- Responsive Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-sm btn-success pull-right" href="javascript: newCitizen()">Add dependant</a>                   
                </div>
                <div class="body">
                    <table id="dependants_table" class="table table-striped table-bordered dt-responsive nowrap dataTable" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Guardian</th>
                            <th>Dependant Type</th>
                            <th>Dependant</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# RESPONSIVE Table -->
<script type="text/javascript">
    function newCitizen()
    {
        document.getElementById("new_form").reset();
        $('.error-class').empty();
        $('#addModal').modal('show');
    }
    function EditModal(id)
    {
        document.getElementById("edit_form").reset();
        $('.error-class').empty();
        var url = base_url + "/api/dependants/"+id;
        $.get(url, function (dependant) {
          $('#edit_form input[id=dependant_id]').val(dependant['id']);
          $('#edit_form input[id=dependant_type]').val(dependant['name']);
          $('#edit_form input[id=dependant]').val(dependant['name']);
          $('#editModal').modal('show');
      });
    }
    

    
</script>

@endsection

@section('modals')
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">New Dependant Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'new_form']) !!}
                @include('forms.dependent')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="savebtn">Save Dependant</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Dependant Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'edit_form']) !!}
            <input type="hidden" name="dependant_id" id="dependant_id">
                @include('forms.dependent')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="updatebtn">Update citizen Type</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#savebtn').click(function(){
    var form = $("#new_form");
    var formData = form.serialize();
    $('.error-class').empty();
    $.ajax({
      url:base_url + "/api/dependants",
      type:'POST',
      data:formData,
      success:function(dat) {
        var error_values = JSON.stringify(dat);
        var data = JSON.parse(error_values);

          if(data['errors']) {
            
            $( '#new_form [id=guardian_error]' ).html(data['errors']['first_name']);
            $( '#new_form [id=dependant_type_error]' ).html(data['errors']['second_name']);
            $( '#new_form [id=dependant_error]' ).html(data['errors']['identity_no']);
          }
          else {
            $('#addModal').modal('hide');
            loaddependants();
            new PNotify({
                text: data['success'],
                type: 'success'
            });
          }
      },
    });
    });
    $('#updatebtn').click(function () {
      var editform = $("#edit_form");
      var formData = editform.serialize();
      var id = document.getElementById("citizen_id").value;
      $('.error-class').empty();
      var url = base_url + "/api/user-types/"+id;
      $.ajax({
          url:url,
          type:'put',
          data:formData,
          success:function(data) {
            // var error_values = JSON.parse(data);
              if(data['errors']) {
               $( '#edit_form [id=name_error]' ).html(data['errors']['name'][0]);

              }
              else {
                $('#editModal').modal('hide');
                loaddependants();
                new PNotify({
                    text: data['success'],
                    type: 'success'
                });
              }
          },
      });
    });
</script>
@endsection