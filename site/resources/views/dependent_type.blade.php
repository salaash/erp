@extends('layouts.main')
{{-- @section('breadcrump')
<div class="page-header">
    <h2>Dependent type</h2>
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="active">Dependent type</li>
    </ol>
</div>
@endsection --}}
@section('main-content')
<script type="text/javascript">
  $(document).ready(function () {
      loaddependenttypes();  
  });

</script>
<script type="text/javascript">
function loaddependenttypes()
  {
    // var url = base_url + "/group-details";
    var url = '{{url('api/dependent-types')}}';
    $.ajax({
      type:'GET',
      url:url,
      success: function(dependent_types)
      {

        // console.log(JSON.stringify(counties));
        var dependent_types_row = '';
        var no = 0;
        $('#dependent_types_table').dataTable().fnClearTable();
        $('#dependent_types_table').dataTable().fnDestroy();
        $.each(dependent_types, function(k, v) {
          no=no+1;
          dependent_types_row+="<tr><td>"+no+"</td><td>"+v.name+"</td><td><div class='btn-group'><button type='button' class='btn btn-success dropdown-toggle btn-xs' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Actions Buttons <span class='caret'></span></button>\n\
              <div class='dropdown-menu'>\n\
                  <ul style='list-style-type:none'><li><a class='dropdown-item' href='javascript:EditModal("+v.id+")'>Edit</a></li></ul>\n\
              </div></div></td></tr>";

             
        }); 
        $("#dependent_types_table").append(dependent_types_row);
        $('#dependent_types_table').DataTable({
          info:true,
          searching:false,
          bFilter:false,
          lengthChange:false
        });
      }
    });
  }

</script>
<!-- Responsive Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-sm btn-success pull-right" href="javascript: newDependentType()">Add dependent type</a>                   
                </div>
                <div class="body">
                    <table id="dependent_types_table" class="table table-striped table-bordered dt-responsive nowrap dataTable" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# RESPONSIVE Table -->
<script type="text/javascript">
    function newDependentType()
    {
        document.getElementById("new_dependent_type_form").reset();
        $('.error-class').empty();
        $('#newDependentTypeModal').modal('show');
    }
    function EditModal(id)
    {
        document.getElementById("edit_dependent_type_form").reset();
        $('.error-class').empty();
        var url = base_url + "/api/dependent-types/"+id;
        $.get(url, function (member) {
          $('#edit_dependent_type_form input[id=dependent_type_id]').val(member['id']);
          $('#edit_dependent_type_form input[id=name]').val(member['name']);

          $('#editDependentTypeModal').modal('show');
      });
    }
    

    
</script>

@endsection

@section('modals')
<div class="modal fade" id="newDependentTypeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">New Dependent Type Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'new_dependent_type_form']) !!}
                @include('forms.dependent_type')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveDependentTypeBtn">Save Dependent Type</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editDependentTypeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Dependent Type Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'edit_dependent_type_form']) !!}
            <input type="hidden" name="dependent_type_id" id="dependent_type_id">
                @include('forms.dependent_type')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="updateDependentTypeBtn">Update Dependent Type</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#saveDependentTypeBtn').click(function(){
    var form = $("#new_dependent_type_form");
    var formData = form.serialize();
    $('.error-class').empty();
    $.ajax({
      url:base_url + "/api/dependent-types",
      type:'POST',
      data:formData,
      success:function(data) {
          if(data['errors']) {
            $( '#new_dependent_type_form [id=name_error]' ).html(data['errors']['name'][0]);
          }
          else {
            $('#newDependentTypeModal').modal('hide');
            loaddependenttypes();
            new PNotify({
                text: data['success'],
                type: 'success'
            });
          }
      },
    });
    });
    $('#updateDependentTypeBtn').click(function () {
      var editform = $("#edit_dependent_type_form");
      var formData = editform.serialize();
      var id = document.getElementById("dependent_type_id").value;
      $('.error-class').empty();
      var url = base_url + "/api/dependent-types/"+id;
      $.ajax({
          url:url,
          type:'put',
          data:formData,
          success:function(data) {
            // var error_values = JSON.parse(data);
              if(data['errors']) {
               $( '#edit_dependent_type_form [id=name_error]' ).html(data['errors']['name'][0]);

              }
              else {
                $('#editDependentTypeModal').modal('hide');
                loaddependenttypes();
                new PNotify({
                    text: data['success'],
                    type: 'success'
                });
              }
          },
      });
    });
</script>
@endsection