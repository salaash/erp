<div class="form-group">
  <div class="col-md-6">
    <label for="school_type">School type</label>
    <select name="school_type" class="form-control" id="school_type"></select>
      <span class="text-danger error-class" id="school_type_error"></span>
  </div>
  <div class="col-md-6">
		<label for="name">School name</label>
    <input type="text" name="name" id="name" class="form-control">
      <span class="text-danger error-class" id="name_error"></span>
  </div>
</div>