<div class="row clearfix">
  <div class="col-md-4">
      <label for="guardian">Gurdian </label>
      <select name="guardian" class="form-control" id="guardian">
    </select>
      <span class="text-danger error-class" id="guardian_error"></span>
  </div>
  <div class="col-md-4">
      <label for="dependant_type">Dependant Type </label>
      <select name="dependant_type" class="form-control" id="dependant_type">
    </select>
      <span class="text-danger error-class" id="dependant_type_error"></span>
  </div>
  <div class="col-md-4">
      <label for="dependant">Dependant </label>
      <select name="dependant" class="form-control" id="dependant">
    </select>
      <span class="text-danger error-class" id="dependant_error"></span>
  </div>
</div>

