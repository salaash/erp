<div class="form-group">
  <div class="col-md-12">
      {!! Form::label('name','County name :') !!}
      {!! Form::text('name', null, array('class'=>'form-control','id'=>'name','placeholder'=>'Enter county name...')) !!}
      <span class="text-danger error-class" id="name_error"></span>
  </div>
</div>
