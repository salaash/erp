<div class="row clearfix">
  <div class="col-md-3">
      {!! Form::label('first_name','First Name') !!}
      {!! Form::text('first_name', null, array('class'=>'form-control','id'=>'first_name')) !!}
      <span class="text-danger error-class" id="first_name_error"></span>
  </div>
  <div class="col-md-3">
      {!! Form::label('second_name','Second Name') !!}
      {!! Form::text('second_name', null, array('class'=>'form-control','id'=>'second_name')) !!}
      <span class="text-danger error-class" id="second_name_error"></span>
  </div>
  <div class="col-md-3">
      {!! Form::label('last_name','Last Name') !!}
      {!! Form::text('last_name', null, array('class'=>'form-control','id'=>'last_name')) !!}
      <span class="text-danger error-class" id="last_name_error"></span>
  </div>
  <div class="col-md-3">
      {!! Form::label('identity_no','ID Number') !!}
      <input type="text" name="identity_no" id="identity_no" class="form-control">
      <span class="text-danger error-class" id="id_no_error"></span>
  </div>
</div>
<div class="row clearfix">
   <div class="col-md-3">
      <label for="phone_number">Phone Number</label>
      <input type="text" name="phone_number" id="phone_number" class="form-control" >
      <span class="text-danger error-class" id="phone_number_error"></span>
  </div>
  <div class="col-md-3">
      <label for="birth">Date of birth</label>
      <input type="date" name="birth" id="birth" class="form-control" >
      <span class="text-danger error-class" id="birth_error"></span>
  </div>
  <div class="col-md-3">
      <label for="birth_no">Birth cert no</label>
      <input type="text" name="birth_no" id="birth_no" class="form-control" >
      <span class="text-danger error-class" id="birth_no_error"></span>
  </div>
  <div class="col-md-3">
      <label for="origin_county">County of origin</label>
	    <select name="origin_county" class="form-control" id="origin_county">
		  {{-- <option value=" " selected="" disabled="">Select county of origin</option> --}}
		</select>
      <span class="text-danger error-class" id="origin_county_error"></span>
  </div>
  
</div>
<div class="row clearfix">
  <div class="col-md-3">
      <label for="current_residence">Current residence</label>
      <select name="current_residence" class="form-control" id="current_residence">
      <option value="" selected="" disabled="">Select county</option>
    </select>
      <span class="text-danger error-class" id="current_residence_error"></span>
  </div>
	<div class="col-md-3">
      <label for="occupation">Occupation</label>
      <input type="text" name="occupation" id="occupation" class="form-control" >
      <span class="text-danger error-class" id="occupation_error"></span>
  </div>
  <div class="col-md-3">
      <label for="nhif_no">NHIF No</label>
      <input type="text" name="nhif_no" id="nhif_no" class="form-control" >
      <span class="text-danger error-class" id="nhif_no_error"></span>
  </div>
  <div class="col-md-3">
      <label for="kra_pin">KRA pin</label>
      <input type="text" name="kra_pin" id="kra_pin" class="form-control">
      <span class="text-danger error-class" id="kra_pin_error"></span>
  </div>
  
</div>
<div class="row clearfix">
  <div class="col-md-2">
      <label for="plot_no">Plot Number</label>
      <input type="text" name="plot_no" id="plot_no" class="form-control">
      <span class="text-danger error-class" id="plot_no_error"></span>
  </div>
  <div class="col-md-2">
      <label for="house_no">House Number</label>
      <input type="text" name="house_no" id="house_no" class="form-control" >
      <span class="text-danger error-class" id="house_no_error"></span>
  </div>
</div>

