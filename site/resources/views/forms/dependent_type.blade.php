<div class="form-group">
  <div class="col-md-12">
      {!! Form::label('name','Dependent type') !!}
      {!! Form::text('name', null, array('class'=>'form-control','id'=>'name','placeholder'=>'Enter dependent type...')) !!}
      <span class="text-danger error-class" id="name_error"></span>
  </div>
</div>
