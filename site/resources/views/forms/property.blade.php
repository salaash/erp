<div class="form-group">
  <div class="col-md-4">
		<label for="user">Citizen</label>
		<select name="user" class="form-control" id="user"></select>
      <span class="text-danger error-class" id="user_error"></span>
  </div>
  <div class="col-md-4">
		<label for="property_type">Property type</label>
		<select name="property_type" class="form-control" id="property_type"></select>
      <span class="text-danger error-class" id="property_type_error"></span>
  </div>
	<div class="col-md-4">
		<label for="property_description">Property description</label>
    <textarea name="property_description" id="property_description" class="form-control">
      
    </textarea>
    {{-- <input type="text" name="property_description" id="property_description" class="form-control"> --}}
      <span class="text-danger error-class" id="property_description_error"></span>
  </div>
</div>
