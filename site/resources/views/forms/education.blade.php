<div class="form-group">
  <div class="col-md-6">
    <label for="citizen">Citizen</label>
    <select name="citizen" class="form-control" id="citizen"></select>
      <span class="text-danger error-class" id="citizen_error"></span>
  </div>
  <div class="col-md-6">
    <label for="school">School</label>
    <select name="school" class="form-control" id="school"></select>
      <span class="text-danger error-class" id="school_error"></span>
  </div>
  
</div>