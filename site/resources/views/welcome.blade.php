@extends('layouts.main')
@section('breadcrump')
<div class="page-header">
    <h2>Dashboard</h2>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}">Dashboard</a></li>
    </ol>
</div>
@endsection
@section('main-content')

<script type="text/javascript">
  $(document).ready(function () {
      countCitizens(); 
      countDependants();
      countSchools();
      countProperties();

  });

  function countCitizens()
  {
    var url = base_url + "/api/count-citizens";
    $.get(url, function (citizens) {
      $("#citizens_count").text(citizens);
    });
  }
  function countDependants()
  {
    var url2 = base_url + "/api/count-dependants";
    $.get(url2, function (dependants) {
      $("#dependants_count").text(dependants);
    });
  }
  function countSchools()
  {
    var url3 = base_url + "/api/count-schools";
    $.get(url3, function (schools) {
      $("#schools_count").text(schools);
    });
  }
  function countProperties()
  {
    var url4 = base_url + "/api/count-properties";
    $.get(url4, function (properties) {
      $("#properties_count").text(properties);
    });
  }

</script>


<div class="row clearfix">
    <div class="col-lg-3">
        <div class="widget bg-blue">
            <div class="col-xs-4 widget-icon">
                <i class="material-icons">perm_identity</i>
            </div>
            <div class="col-xs-8 widget-body text-right">
                <span> Citizens </span>
                <h2 id="citizens_count"></h2>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="widget bg-light-blue">
            <div class="col-xs-4 widget-icon">
                <i class="material-icons">group</i>
            </div>
            <div class="col-xs-8 widget-body text-right">
                <span> Dependants </span>
                <h2 class="num" id="dependants_count"></h2>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="widget bg-orange">
            <div class="col-xs-4 widget-icon">
                <i class="material-icons">schools</i>
            </div>
            <div class="col-xs-8 widget-body text-right">
                <span> Schools </span>
                <h2 class="num" id="schools_count"></h2>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="widget bg-deep-orange">
            <div class="col-xs-4 widget-icon">
                <i class="material-icons">account_balance</i>
            </div>
            <div class="col-xs-8 widget-body text-right">
                <span> Properties </span>
                <h2 class="num" id="properties_count"></h2>
            </div>
        </div>
    </div>
</div>
@endsection