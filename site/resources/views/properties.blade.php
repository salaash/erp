@extends('layouts.main')

@section('main-content')
<script type="text/javascript">
  $(document).ready(function () {
      loadProperties(); 

    var url = base_url + "/api/citizens";
    var citizen = " ";
      $.getJSON(url, function (citizens) {
        citizen+="<option value=''>"+'Select citizen'+"</option>";
        $.each(citizens,function(index,item) 
        {
          citizen+="<option value='"+item.user_id+"'>"+item.first_name+" "+item.second_name+" "+item.last_name+"</option>";
        });
        $("#user").html(citizen);
      });  

      var url2 = base_url + "/api/property-types";
      var properties = " ";
      $.getJSON(url2, function (property_types) {
        properties+="<option value=''>"+'Select property type'+"</option>";
        $.each(property_types,function(index,item) 
        {
          properties+="<option value='"+item.property_type_id+"'>"+item.name+"</option>";
        });
        $("#property_type").html(properties);
      }); 


      // populating select fields for edit
      var edit_properties = " ";
      $.getJSON(url2, function (edit_property_types) {
        edit_properties+="<option value=''>"+'Select property type'+"</option>";
        $.each(edit_property_types,function(index,item) 
        {
          edit_properties+="<option value='"+item.property_type_id+"'>"+item.name+"</option>";
        });
        $("#edit_form select[id=property_type]").html(edit_properties);
      });

      var edit_citizen = " ";
      $.getJSON(url, function (edit_citizens) {
        edit_citizen+="<option value=''>"+'Select citizen'+"</option>";
        $.each(edit_citizens,function(index,item) 
        {
          edit_citizen+="<option value='"+item.user_id+"'>"+item.first_name+" "+item.second_name+" "+item.last_name+"</option>";
        });
        $("#edit_form select[id=user]").html(edit_citizen);
      }); 
  });

</script>
<script type="text/javascript">
function loadProperties()
  {
    // var url = base_url + "/group-details";property-types
    var url = '{{url('api/properties')}}';
    $.ajax({
      type:'GET',
      url:url,
      success: function(properties)
      {
        // console.log(JSON.stringify(counties));
        var user_types_row = '';
        var no = 0;
        $('#properties_table').dataTable().fnClearTable();
        $('#properties_table').dataTable().fnDestroy();
        $.each(properties, function(k, v) {
          no=no+1;
          user_types_row+="<tr><td>"+no+"</td><td>"+v.first_name+" "+v.second_name+" "+v.last_name+"</td><td>"+v.name+"</td><td>"+v.property_description+"</td><td><div class='btn-group'><button type='button' class='btn btn-success dropdown-toggle btn-xs' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Actions Buttons <span class='caret'></span></button>\n\
              <div class='dropdown-menu'>\n\
                  <ul style='list-style-type:none'><li><a class='dropdown-item' href='javascript:EditModalfn("+v.test+")'>Edit</a></li></ul>\n\
              </div></div></td></tr>";

             
        }); 
        $("#properties_table").append(user_types_row);
        $('#properties_table').DataTable({
          info:true,
          searching:false,
          bFilter:false,
          lengthChange:false
        });
      }
    });
  }

</script>
<!-- Responsive Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-sm btn-success pull-right" href="javascript: newproperty()">Add property</a>                   
                </div>
                <div class="body">
                    <table id="properties_table" class="table table-striped table-bordered dt-responsive nowrap dataTable" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Citizen</th>
                            <th>Property Type</th>
                            <th>Property description</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# RESPONSIVE Table -->
@endsection

@section('modals')
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">New Property Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'new_form']) !!}
                @include('forms.property')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="savebtn">Save Property</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Property Information</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['class'=>'form-horizontal','role'=>'form','id'=>'edit_form']) !!}
            <input type="hidden" name="property_id" id="property_id">
                @include('forms.property')
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="updatebtn">Update User Type</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function newproperty()
    {
        document.getElementById("new_form").reset();
        document.getElementById("savebtn").disabled = false;
        $('.error-class').empty();
        $('#addModal').modal('show');
    }
    function EditModalfn(id)
    {
        document.getElementById("edit_form").reset();
        document.getElementById("updatebtn").disabled = false;
        $('.error-class').empty();
        var url = base_url + "/api/properties/"+id;
        $.get(url, function (property) {
          $('#edit_form input[id=property_id]').val(property[0].id);

          $('#edit_form select[id=user]').val(property[0].user_id).change();
          $('#edit_form select[id=property_type]').val(property[0].property_type_id).change();

          $('#edit_form textarea[id=property_description]').val(property[0].property_description);

          $('#editModal').modal('show');
      });
    }


    $('#savebtn').click(function(){
    var form = $("#new_form");
    var formData = form.serialize();
    $('.error-class').empty();
    document.getElementById("savebtn").disabled = true;
    $.ajax({
      url:base_url + "/api/properties",
      type:'POST',
      data:formData,
      success:function(dat) {
        var error_values = JSON.stringify(dat);
        var data = JSON.parse(error_values);
          if(data['errors']) {
            $( '#new_form [id=user_error]' ).html(data['errors']['user']);
            $( '#new_form [id=property_type_error]' ).html(data['errors']['property_type']);
            $( '#new_form [id=property_description_error]' ).html(data['errors']['property_description']);
          }
          else {
            $('#addModal').modal('hide');
            loadProperties();
            new PNotify({
                text: data['success'],
                type: 'success'
            });
          }
      },
    });
    });
    $('#updatebtn').click(function () {
      var editform = $("#edit_form");
      var formData = editform.serialize();
      var id = document.getElementById("property_id").value;
      document.getElementById("updatebtn").disabled = true;
      $('.error-class').empty();
      var url = base_url + "/api/properties/"+id;
      $.ajax({
          url:url,
          type:'put',
          data:formData,
          success:function(dat) {
            var error_values = JSON.stringify(dat);
            var data = JSON.parse(error_values);
              if(data['errors']) {
                $( '#edit_form [id=user_error]' ).html(data['errors']['user']);
                $( '#edit_form [id=property_error]' ).html(data['errors']['property']);
                $( '#edit_form [id=property_description_error]' ).html(data['errors']['property_description']);

              }
              else {
                $('#editModal').modal('hide');
                loadProperties();
                new PNotify({
                    text: data['success'],
                    type: 'success'
                });
              }
          },
      });
    });
</script>
@endsection