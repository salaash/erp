<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('first_name');
            $table->string('second_name');
            $table->string('last_name')->nullable();
            $table->string('user_type_id')->nullable();
            $table->string('identity_no')->nullable();
            $table->string('birth_no')->nullable();
            $table->string('kra_pin')->nullable();
            $table->string('nhif_pin')->nullable();
            $table->string('county_id');
            $table->string('plot_no_id')->nullable();
            $table->string('house_no')->nullable();
            $table->string('origin_county_id');
            $table->date('birth');//date of birth
            $table->string('occupation')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
