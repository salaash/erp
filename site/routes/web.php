<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
Route::view('schools-view','school');
Route::view('counties-view','counties');
Route::view('dependent-type','dependent_type');
Route::view('property-type','property_type');
Route::view('user-type','user_type');
Route::view('properties-view','properties');
Route::view('citizens','citizens');
Route::get('citizen-profile/{citizen_id}','HomeController@profile')->name('citizen-profile');
Route::view('school-type-view','school_type');
Route::view('education-view','education');
Route::view('dependant-view','dependent');
Route::view('/', 'welcome')->name('home');
});
