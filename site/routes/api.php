<?php

use Illuminate\Http\Request;

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::apiResources([
	'counties'=>'Api\CountyController',
	'dependent-types'=>'Api\DependentTypeController',
	'property-types'=>'Api\PropertyTypeController',
	'user-types'=>'Api\UserTypeController',
	'citizens'=>'Api\CitizenController',
	'properties'=>'Api\PropertyController',
	'school-types'=>'Api\SchoolTypeController',
	'schools'=>'Api\SchoolController',
	'education'=>'Api\EducationController',
	'dependants'=>'Api\DependentController'
]);
Route::post('get-dependants','Api\DependentController@get_dependants');
Route::post('load-dependants','Api\DependentController@load_dependants');

Route::post('dependants-list','Api\CitizenController@citizen_dependants');
Route::post('dependants-count','Api\CitizenController@count_dependants');
Route::post('get-citizen-details','Api\CitizenController@get_details');
Route::post('properties-list','Api\CitizenController@citizen_properties');
Route::post('properties-count','Api\CitizenController@count_properties');
Route::post('education-list','Api\CitizenController@citizen_education');

Route::get('count-properties','Api\PropertyController@count');
Route::get('count-schools','Api\SchoolController@count');
Route::get('count-citizens','Api\CitizenController@count');
Route::get('count-dependants','Api\DependentController@count');
